import { readdirSync } from 'fs';
import childprocess from 'child_process';

export const exec = (command: any) => {
  return new Promise((resolve, reject) => {
    const childProcess = childprocess.exec(
      command,
      { maxBuffer: 1024 * 10000 },
      (error, result) => {
        if (error) {
          reject(error);
        } else {
          resolve(result);
        }
      });
    childProcess.stdout?.pipe(process.stdout);
    childProcess.stderr?.pipe(process.stderr);
  });
};

export const getDirectories = (source: string) => {
  return readdirSync(source, { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
    .map((dirent) => dirent.name);
};

/**
 * @param {String} stringTemplate
 * @param {Object} keyPairs
 * @return {String}
 * **/
export const interpolateString = (stringTemplate: string, keyPairs: { [key: string]: any }): string => {
  return stringTemplate.replace(
    new RegExp(`\\$\{([^\{(.*?)\}]+)\}`, `g`),
    (_unused, key) => {
      return keyPairs[key];
    });
};

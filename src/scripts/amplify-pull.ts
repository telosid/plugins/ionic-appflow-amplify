import { exec } from '../lib';

const {
  _AWS_ACCESS_KEY_ID: accessKeyId,
  _AWS_SECRET_ACCESS_KEY: secretAccessKey,
  _AWS_REGION: region,
  AMPLIFY_PROJECT_NAME: projectName,
  AMPLIFY_APP_ID: appId,
  AMPLIFY_ENV: envName,
  AWS_PROFILE: profile,
  AMPLIFY_CLI_VERSION: amplifyCliVersion = '10.2.3',
  AMPLIFY_CLI_COMMAND: amplifyCliCommand = 'pull'
} = process.env;

const awscloudformation = {
  configLevel: "project",
  useProfile: Boolean(profile),
  ...(profile ? {profileName: profile} : {}),
  ...(profile ? {} : {accessKeyId: accessKeyId}),
  ...(profile ? {} : {secretAccessKey: secretAccessKey}),
  ...(profile ? {} : {region: region})
};

const amplify = JSON.stringify({
  projectName,
  appId,
  envName,
  defaultEditor: "code"
});

const providers = JSON.stringify({ awscloudformation });

const amplifyString = JSON.stringify(amplify);
const providersString = JSON.stringify(providers);

const amplifyCommand = `amplify ${amplifyCliCommand} --amplify ${amplifyString} --providers ${providersString} --yes`;
const command = `NPM_CONFIG_UNSAFE_PERM=true npx -p @aws-amplify/cli@${amplifyCliVersion} -c '${amplifyCommand}'`;
console.log(`command to execute:`, command);

exec(command).then((result) => {
  console.log(`Command result`, result);
}).catch((error) => {
  console.error(`Command Error`, error);
  process.exitCode = 1;
});

